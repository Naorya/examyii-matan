<?php

use yii\db\Migration;

class m170720_055829_add_category_table extends Migration
{
    public function up()
    {
		$this->createTable('category', [
            'id' => $this->primaryKey(),
            'category_name' => $this->string(),
		]);
    }

    public function down()
    {
        $this->dropTable('category');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
