<?php

use yii\db\Migration;

class m170715_085846_add_user_table extends Migration
{
    public function up()
    {
      $this->createTable('user', [
            'id' => $this->primaryKey(),
            'firstname' => $this->string(),
            'lastname' => $this->string(),
            'username' => $this->string(),
            'password' => $this->string(),
            'authKey' => $this->string(),
      ]);
    }

    public function down()
    {
      $this->dropTable('user');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
