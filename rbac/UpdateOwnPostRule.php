<?php 

	namespace app\rbac;

	use yii\rbac\Rule;
	use app\models\Post;
	use yii\web\NotFoundHttpException;

	/**
	 * Checks if authorID matches user passed via params
	 */
	class UpdateOwnPostRule extends Rule
	{
		public $name = 'UpdateOwnPost';

		/**
		 * @param string|int $user the user ID.
		 * @param Item $item the role or permission that this rule is associated with
		 * @param array $params parameters passed to ManagerInterface::checkAccess().
		 * @return bool a value indicating whether the rule permits the role or permission it is associated with.
		 */
		public function execute($user, $item, $params)
		{	
			$currentPost = Post::findOne($_GET['id']);
			
			if(isset($currentPost)){
				$author = $currentPost->author;
				
				if($author == $user){
					return true;
				}
			}
		
		
			return false;
		}
	}
	
?>